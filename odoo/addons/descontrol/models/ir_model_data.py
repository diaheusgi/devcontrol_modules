from odoo import api, models

class IrModelData(models.TransientModel):
    _name = 'descontrol.module'
    _description = 'Module for adding initial data'

    @api.model
    def import_bank_data(self):
        bank_data_wizard = self.sudo().env["l10n.es.partner.import.wizard"].create({})
        bank_data_wizard.execute()

    @api.model
    def install_languages(self):
        installer = self.sudo().env['base.language.install'].create({'lang': 'es_ES'})
        installer.lang_install()
        installer = self.sudo().env['base.language.install'].create({'lang': 'ca_ES'})
        installer.lang_install()

    @api.model
    def disable_company_noupdate(self):
        company_imd = self.env['ir.model.data'].search([
            ('name', '=', 'main_company')
        ])
        company_imd.noupdate = False
